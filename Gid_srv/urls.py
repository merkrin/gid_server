
from django.contrib import admin
from django.urls import path, include
from django_registration.backends.activation.views import RegistrationView

from users.forms import UserForm

urlpatterns = [
    path('api/games/', include('games.urls')),
    path('api/users/', include('users.urls')),
    path('admin/', admin.site.urls),
    path('api/auth/', include('djoser.urls.authtoken'))]
