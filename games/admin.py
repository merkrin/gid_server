from django.contrib import admin

from django.contrib import admin
from .models import Game, Discount, Comment, Shop, Price, Article

admin.site.register(Game)
admin.site.register(Discount)
admin.site.register(Comment)
admin.site.register(Shop)
admin.site.register(Price)
admin.site.register(Article)

