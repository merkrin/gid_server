from django.core.management.base import BaseCommand
from games import steam_loader


class Command(BaseCommand):
    def handle(self, *args, **options):
        steam_loader.add_description()