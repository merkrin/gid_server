from django.core.management.base import BaseCommand
from steam_crawler.steam_crawler.spiders.steamcrawler import SteamcrawlerSpider
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings


class Command(BaseCommand):
    def handle(self, *args, **options):
        process = CrawlerProcess({
            'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)',
            'DOWNLOAD_DELAY': 2  # actual delay will vary between 1 and 3 seconds
                                 # which is sufficient because we require about 1.5
        })
        process.crawl(SteamcrawlerSpider)
        process.start()
