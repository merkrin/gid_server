from django.core.management.base import BaseCommand
from steam_crawler.steam_crawler.spiders.steam_news_crawler import SteamNewsCrawlerSpider
from scrapy.crawler import CrawlerProcess
from games.steam_articles_loader import cleanup


class Command(BaseCommand):
    def handle(self, *args, **options):
        process = CrawlerProcess({
            'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)',
        })
        process.crawl(SteamNewsCrawlerSpider)
        process.start()
        # removing news that are more than half a year old
        cleanup()


