from django.core.management.base import BaseCommand
from games.models import Discount, Price


class Command(BaseCommand):
    def handle(self, *args, **options):
        discounts = Discount.objects.filter(percent=0)
        prices = Price.objects.all()

        step = 0
        for price in prices:
            print("step " + str(step))

            if price.discount and price.discount.percent == 0:
                price.discount = None
                price.save()
            step+=1

        discounts.delete()