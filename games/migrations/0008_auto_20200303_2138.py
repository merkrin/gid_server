# Generated by Django 3.0.2 on 2020-03-03 21:38

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0007_game_steam_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='discount',
            name='percent',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='game',
            name='developers',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(default='', max_length=100), default=[], size=None),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='game',
            name='header_image',
            field=models.CharField(default='', max_length=500),
        ),
        migrations.AddField(
            model_name='game',
            name='is_free',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='game',
            name='required_age',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='game',
            name='screenshots',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(default='', max_length=500), default=[], size=None),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='game',
            name='trailer_480',
            field=models.CharField(default='', max_length=300),
        ),
        migrations.AddField(
            model_name='game',
            name='trailer_image',
            field=models.CharField(default='', max_length=300),
        ),
        migrations.AddField(
            model_name='price',
            name='currency',
            field=models.CharField(default='RUB', max_length=20),
        ),
        migrations.AddField(
            model_name='price',
            name='final_formatted',
            field=models.CharField(default='', max_length=30),
        ),
        migrations.AddField(
            model_name='price',
            name='initial_formatted',
            field=models.CharField(default='', max_length=30),
        ),
    ]
