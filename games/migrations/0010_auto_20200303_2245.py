# Generated by Django 3.0.2 on 2020-03-03 22:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0009_auto_20200303_2146'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='min_requirements',
            field=models.CharField(default='', max_length=500),
        ),
        migrations.AddField(
            model_name='game',
            name='rec_requirements',
            field=models.CharField(default='', max_length=500),
        ),
    ]
