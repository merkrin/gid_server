from django.db import models
from django.contrib.postgres.fields import ArrayField
import datetime


# may be redundant in future
class RangedFloat(models.FloatField):
    def __init__(self, min_value=None, max_value=None,
                 *args, **kwargs):
        self.min_value, self.max_value = min_value, max_value
        super(RangedFloat, self).__init__(*args, **kwargs)

    def formfield(self, **kwargs):
        defaults = {'min_value': self.min_value,
                    'max_value': self.max_value}
        defaults.update(**kwargs)
        return super(RangedFloat, self).formfield(**defaults)


# may be redundant in future
class RangedInteger(models.FloatField):
    def __init__(self, min_value=None, max_value=None,
                 *args, **kwargs):
        self.min_value, self.max_value = min_value, max_value
        super(RangedInteger, self).__init__(*args, **kwargs)

    def formfield(self, **kwargs):
        defaults = {'min_value': self.min_value,
                    'max_value': self.max_value}
        defaults.update(**kwargs)
        return super(RangedInteger, self).formfield(**defaults)


class Platform(models.TextChoices):
    PS4 = ('ps4', 'PlayStation 4')
    PS3 = ('ps3', 'Playstation 3')
    XB1 = ('xbox1', 'Xbox One')
    XB360 = ('xbox360', 'Xbox 360')
    NINTENDO = ('nintendo', 'Nintendo Switch')
    MAC = ('mac', 'Mac')
    WINDOWS = ('windows', 'Windows')
    LINUX = ('linux', 'Linux')


class Comment(models.Model):
    content = models.CharField(max_length=300, default="")
    mark = RangedFloat(min_value=0, max_value=10, default=0)
    game = models.ForeignKey('Game', on_delete=models.CASCADE, null=True)
    user_id = models.PositiveIntegerField(default=0)

    def __str__(self):
        return "Comment on " + self.game.name + " by user " + str(self.user_id)


class Game(models.Model):
    name = models.CharField(max_length=500, default="")
    genre = ArrayField(models.CharField(max_length=200, default=""), blank=True, default=list)
    about = models.CharField(max_length=5000, default="")
    detailed_description=models.CharField(max_length=25000, default="")
    release_date = models.CharField(max_length=1000, default="")
    rating = RangedFloat(min_value=0, max_value=10, default=0)
    marks_numbers = ArrayField(models.PositiveIntegerField(default=0), null=True)
    state = models.CharField(max_length=100, default="")
    platforms = ArrayField(models.CharField(max_length=20, choices=Platform.choices), default=list)
    popularity = models.PositiveIntegerField(default=0)
    steam_id = models.BigIntegerField()
    header_image = models.CharField(max_length=500, default="")
    screenshots = ArrayField(models.CharField(max_length=500, default=""), null=True)
    required_age = models.IntegerField(default=0)
    is_free = models.BooleanField(default=False)
    trailer_480 = models.CharField(max_length=300, default="")
    trailer_image = models.CharField(max_length=300, default="")
    developers = ArrayField(models.CharField(max_length=500, default=""), default=list)
    min_requirements = models.CharField(max_length=1500, default="")
    rec_requirements = models.CharField(max_length=1500, default="")

    def __str__(self):
        return self.name


class Shop(models.Model):
    name = models.CharField(max_length=50, default='')
    games = models.ManyToManyField(Game, blank=True, through='Price')

    def __str__(self):
        return self.name


class Price(models.Model):
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE)
    price = models.FloatField(default=0)
    discount = models.OneToOneField('Discount', null=True, on_delete=models.PROTECT,
                                    blank=True)
    currency = models.CharField(default="RUB", max_length=20)
    initial_formatted = models.CharField(default="", max_length=30)
    final_formatted = models.CharField(default="", max_length=30)

    def __str__(self):
        return str(self.price)


class Discount(models.Model):
    old_price = models.PositiveIntegerField(default=0)
    new_price = models.PositiveIntegerField(default=0)
    percent = models.PositiveIntegerField(default=0)

    def __str__(self):
        # probably to be changed later
        return "discount size: " + str(self.old_price - self.new_price)


class Article(models.Model):
    title = models.CharField(default="Untitled", max_length=200)
    url = models.CharField(default="", max_length=200)
    author = models.CharField(default="", max_length=100)
    preview = models.CharField(default="No preview available", max_length=2000)
    timestamp = models.DateTimeField(default=datetime.datetime.now())
    game = models.ForeignKey(Game, on_delete=models.CASCADE)

    def __str__(self):
        return "Article \"" + self.title + "\" about game " + self.game.name
