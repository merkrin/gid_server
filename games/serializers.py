from rest_framework import serializers

from .models import Game, Comment, Discount, Shop, Price, Article


class GameSerializer(serializers.ModelSerializer):
    price_set = serializers.StringRelatedField(many=True)
    shop_set = serializers.StringRelatedField(many=True)

    class Meta:
        model = Game
        fields = (
        'id', 'name', 'genre', 'shop_set', 'price_set', 'about', 'release_date', 'rating', 'marks_numbers', 'state', 'platforms',
        'popularity', 'header_image', 'is_free', 'screenshots', 'required_age', 'trailer_480', 'trailer_image',
        'developers', 'min_requirements', 'rec_requirements', 'detailed_description')


class CommentSerializer(serializers.ModelSerializer):
    game = GameSerializer(required=False)

    class Meta:
        model = Comment
        fields = ('id', 'content', 'mark', 'game', 'user_id')


class ShopSerializer(serializers.ModelSerializer):
    games = GameSerializer(many=True)

    class Meta:
        model = Shop
        fields = ('id', 'name', 'games')


class SimpleShopSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shop
        fields = ('id', 'name')


class PriceSerializer(serializers.ModelSerializer):
    game = GameSerializer(required=True)
    shop = SimpleShopSerializer(required=True)

    class Meta:
        model = Price
        fields = ('id', 'price', 'game', 'shop', 'currency', 'initial_formatted', 'final_formatted')


class DiscountSerializer(serializers.ModelSerializer):
    price = PriceSerializer(required=True)

    class Meta:
        model = Discount
        fields = ('id', 'old_price', 'new_price', 'price', 'percent')


class ArticleSerializer(serializers.ModelSerializer):
    game = GameSerializer(required=True)

    class Meta:
        model = Article
        fields = ('title', 'url', 'author', 'preview', 'timestamp', 'game')
