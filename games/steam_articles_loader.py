from games.models import Article, Game
from datetime import datetime
from Gid_srv.logger import base_logger

logger = base_logger.getChild('steam_articles_loader')


def parse_article_json(json, game_id):
    logger.info("Loading article")
    time_passed = datetime.now() - datetime.fromtimestamp(json.get('date'))
    if time_passed.days > 183:
        logger.warning("The article is outdated and won't be processed")
        return

    try:
        game = Game.objects.get(steam_id=game_id)
    except Game.DoesNotExist:
        logger.error("No such game in database")
        return
    article = Article.objects.get_or_create(game=game)[0]

    article.title = json.get('title')
    if json.get('author') != '':
        article.author = json.get('author')
    article.url = json.get('url')

    preview = json.get('contents')
    # removing extra links
    while "<a" in preview:
        start = preview.find("<a")
        end = preview.find("</a>")
        preview = preview[0:start:] + preview[(end + 4)::]
    article.preview = preview

    if json.get('date') != 0:
        article.timestamp = datetime.fromtimestamp(json.get('date'))

    article.save()
    logger.info(str(article) + " saved")


def cleanup():
    articles = Article.objects.all()
    for article in articles:
        time_passed = datetime.now() - article.timestamp.replace(tzinfo=None)
        # half a year, might be changed
        if time_passed.days > 183:
            logger.info(str(article) + " is outdated: deleting")
            article.delete()

