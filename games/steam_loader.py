# -*- coding: utf-8 -*-
from django.db import DataError

from games.models import Game, Shop, Price, Discount
from Gid_srv.config import game_ids, game_info, steam_key, log_file
import requests
import time
from Gid_srv.logger import base_logger

logger = base_logger.getChild('steam_loader')


def parse_game_json(json):
    data = json.get("data")
    if data is None or data.get("type") != "game":
        if data is not None:
            logger.warning("Not a game: " + str(data.get("type")))
        else:
            logger.warning("Received no data")
        return []

    steam_id = data.get("steam_appid")
    game = Game.objects.all().get_or_create(steam_id=steam_id)[0]
    game.name = data.get("name")

    logger.info("Loading " + str(steam_id))

    genres = []
    genre_list = data.get("genres")
    if genre_list:
        for g in genre_list:
            genres.append(g.get("description"))
    game.genres = genres

    game.about = data.get("short_description")
    game.detailed = data.get("detailed_description")

    if data.get("release_date") is not None:
        release_date = data.get("release_date").get("date")
        if data.get("release_date").get("coming_soon"):
            state = "Released"
        else:
            state = "Not released yet"
        game.release_date = release_date
        game.state = state

    platforms = []
    platforms_list = data.get("platforms")
    if platforms_list.get("windows"):
        platforms.append('windows')
    if platforms_list.get("mac"):
        platforms.append('mac')
    if platforms_list.get("linux"):
        platforms.append('linux')
    game.platforms = platforms

    developers = data.get("developers")
    devs = []
    if developers:
        for dev in developers:
            devs.append(dev)
    game.developers = devs

    screenshots = []
    s = data.get("screenshots")
    if s:
        for i in range(len(s)):
            shot = s[i].get("path_full")
            screenshots.append(shot)
        game.screenshots = screenshots

    movies = data.get("movies")
    if movies:
        movie = data.get("movies")[0]
        if movie:
            if movie.get("thumbnail"):
                game.trailer_image = movie.get("thumbnail")
            if movie.get("webm").get("480"):
                game.trailer_480 = movie.get("webm").get("480")

    if data.get("required_age"):
        game.required_age = data.get("required_age")

    img = data.get('header_image')
    if img:
        game.header_image = img

    if data.get("is_free"):
        game.is_free = data.get("is_free")

    r = data.get("pc_requirements")
    if r:
        min = r.get("minimum")
        if min:
            game.min_requirements = min
        rec = r.get("recommended")
        if rec:
            game.rec_requirements = rec

    price = data.get("price_overview")
    if price:
        currency = price.get("currency")
        discount = price.get("discount_percent")
        initial = int(price.get("initial")) / 100
        final = int(price.get("final")) / 100
        initial_f = price.get("initial_formatted")
        final_f = price.get("final_formatted")

        steam = Shop.objects.all().get(name="Steam")

        price_model = Price.objects.get_or_create(shop=steam, game=game)[0]

        discount_model = Discount.objects.get_or_create(price=price_model)[0]

        if discount == 0:
            discount_model = None
        else:
            discount_model.old_price = initial
            discount_model.new_price = final
            discount_model.percent = discount

        price_model.discount = discount_model
        price_model.currency = currency
        price_model.initial_formatted = initial_f
        price_model.final_formatted = final_f
        price_model.price = final

    try:
        game.save()
        logger.info("Game " + str(game) + " saved")
    except DataError as e:
        logger.error("game is too big " + str(steam_id))
        with open("corrupted.txt", 'w') as file:
            file.write(str(steam_id) + " ")
        return

    if price:
        if discount_model:
            discount_model.save()
        price_model.save()
        game.shop_set.add(steam)
    try:
        game.save()
        logger.info("Game " + str(game) + " saved")
    except DataError as e:
        logger.error("game is too big " + str(steam_id))
        with open("corrupted.txt", 'w') as file:
            file.write(str(steam_id) + " ")
        return


def get_all_game_ids_steam():
    response = requests.get(game_ids)
    if response.status_code != 200:
        logger.error("Failed to load: status code " + str(response.status_code))
        return []
    response_json = response.json()
    ids = []

    apps = response_json.get("applist").get("apps")
    if len(apps) == 0:
        logger.warning("No games available now")
        return []
    # for i in range(len(response_json.get("applist").get("apps"))):
    for i in range(len(apps)):
        ids.append(apps[i].get("appid"))
    return ids


def get_game_by_id_steam(id):
    print("waiting for response...")
    response = None
    while not response:
        response = requests.get(game_info,
                                params={'appids': id,
                                        'filters': 'basic,short_description,platforms,price_overview,' +
                                                   'metacritic,release_date,genres,developers,' +
                                                   'is_full,screenshots,movies,required_age,header_image,' +
                                                   'pc_requirements,detailed_description,',
                                        'l': 'english'})
        if not response:
            logger.info("Wait for available games...")
            time.sleep(30)

    return list(response.json().values())[0]


def get_ids():
    id = get_all_game_ids_steam()
    logger.info("waiting for response...")
    response = requests.get("http://store.steampowered.com/api/appdetails",
                            params={'appids': id,
                                    'filters': 'basic,short_description,platforms,price_overview,' +
                                               'metacritic,release_date,genres,metacritic',
                                    'l': 'english'})
    print(response.json())
    # return list(response.json().values())[0]


def upload():
    ids = get_all_game_ids_steam()
    for i in range(len(ids)):
        logger.info("get game " + str(i))
        steam_game = get_game_by_id_steam(ids[i])
        if steam_game.get("success"):
            parse_game_json(steam_game)


def add_description():
    games = Game.objects.all()
    logger.info("Getting detailed description for all games")
    for game in games:
        response = None
        while not response:
            logger.info("waiting for response " + str(game.steam_id))
            response = requests.get(game_info,
                                    params={'appids': str(game.steam_id),
                                            'filters': 'basic,detailed_description,',
                                            'l': 'english'})
            if not response:
                logger.info("Wait for available games...")
                time.sleep(30)
        data = (list(response.json().values())[0])
        data = data.get("data")
        game.detailed_description = data.get("detailed_description")
        game.save()
        logger.info(str(game) + " saved")
