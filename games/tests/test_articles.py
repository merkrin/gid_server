from django.test import TestCase
from django.test import Client

from games.models import Game, Article
from users.models import User

c = Client()


class ArticleTestCase(TestCase):
    fixtures = ['games/tests/fixtures/testgames.json']

    game_set = Game.objects.all()
    user = None

    def setUp(self):
        self.user = User.objects.create(email="email@email.com", username="test_user",
                                   password="password")
        c.force_login(self.user)
        i = 0
        for game in self.game_set:
            Article.objects.create(title='article ' + str(i), game=game)
            i += 1

    def test_all_articles(self):
        response = c.get('/api/games/articles/?limit=30')
        self.assertEqual(response.status_code, 200)
        articles = response.json().get('results')
        self.assertEqual(len(articles), len(self.game_set))
        for article in articles:
            game = article.get('game')
            existing = Game.objects.filter(name=game.get('name'))
            self.assertNotEqual(existing, None)

    def test_articles_for_game(self):
        for game in self.game_set:
            id = game.steam_id
            response = c.get('/api/games/articles/', {'game': str(id), 'limit': '30'})
            self.assertEqual(response.status_code, 200)
            articles = response.json().get('results')
            for article in articles:
                received_game = article.get('game')
                self.assertEqual(received_game.get('name'), game.name)

    def test_articles_for_user(self):
        response = c.get('/api/games/articles/?personal&limit=30')
        self.assertEqual(response.status_code, 200)
        articles = response.json().get('results')
        for article in articles:
            game = article.get('game')
            wished_by = User.objects.get(wished_games__name=game.get('name'))
            self.assertEquals(wished_by, self.user)