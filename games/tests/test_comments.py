from django.test import TestCase
from django.test import Client

from games.models import Game, Comment
from users.models import User

c = Client()


class CommentTestCase(TestCase):
    fixtures = ['games/tests/fixtures/testgames.json']

    user = None
    game_set = Game.objects.all()

    def setUp(self):
        self.user = User.objects.create(email="email@email.com", username="test_user",
                                        password="password")
        c.force_login(self.user)
        for game in self.game_set:
            comm = Comment.objects.create(content='a comment', game=game, user_id=self.user.id)

    def test_comments_on_game(self):
        for game in self.game_set:
            response = c.get('/api/games/all/' + str(game.id) + '/comments/')
            self.assertEquals(response.status_code, 200)
            comments = response.json()
            for comment in comments:
                self.assertEquals(comment.get('game').get('name'), game.name)

    def test_comments_for_user(self):
        response = c.get('/api/games/all/comments/personal/')
        self.assertEquals(response.status_code, 200)
        comments = response.json()
        self.assertEquals(len(comments), len(self.game_set))
        for comment in comments:
            self.assertEquals(comment.get('content'), 'a comment')
