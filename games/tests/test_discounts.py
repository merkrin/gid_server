from django.test import TestCase
from django.test import Client

from random import uniform

from games.models import Game, Comment, Discount, Price, Shop
from users.models import User

c = Client()


class DiscountTestCase(TestCase):
    fixtures = ['games/tests/fixtures/testgames.json']

    user = None
    game_set = Game.objects.all()

    def setUp(self):
        self.user = User.objects.create(email="email@email.com", username="test_user",
                                        password="password")
        c.force_login(self.user)
        steam = Shop.objects.get(name='Steam')
        for game in self.game_set:
            old_price = uniform(200, 300)
            new_price = uniform(50, 100)
            percent = (1 - new_price/old_price) * 100
            discount = Discount.objects.create(old_price=old_price, new_price=new_price, percent=percent)
            Price.objects.create(game=game, shop=steam, price=old_price, discount=discount)

        for i in range(10):
            self.user.wished_games.add(self.game_set[i])

    def test_all_discounts(self):
        response = c.get('/api/games/discounts/?limit=30')
        self.assertEqual(response.status_code, 200)
        discounts = response.json().get('results')
        self.assertEqual(len(discounts), len(self.game_set))
        for discount in discounts:
            self.assertGreater(discount.get('percent'), 0)
            self.assertLess(discount.get('percent'), 100)

    def test_personal_discounts(self):
        response = c.get('/api/games/discounts/?personal&limit=30')
        self.assertEqual(response.status_code, 200)
        discounts = response.json().get('results')
        self.assertEqual(len(discounts), 10)
        for discount in discounts:
            game = discount.get('price').get('game')
            wished_by = User.objects.get(wished_games__name=game.get('name'))
            self.assertEquals(wished_by, self.user)