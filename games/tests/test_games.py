from django.test import TestCase
from django.test import Client

from random import randint

from users.models import User
from games.models import Game, Price, Shop

c = Client()

genres = ["Indie", "Action", "Casual"]
names = ['Millenium', 'a', 'Vault Cracker', 'gggggggg']


class GameTestCase(TestCase):
    fixtures = ['games/tests/fixtures/testgames.json']

    def setUp(self):
        user = User.objects.create(email="email@email.com", username="test_user",
                                   password="password")
        c.force_login(user)
        game_set = Game.objects.all()
        steam = Shop.objects.get(name='Steam')
        for game in game_set:
            price = randint(100, 10000)
            Price.objects.create(game=game, shop=steam, price=price)


    def test_all_games(self):
        response = c.get('/api/games/all/?limit=30')
        self.assertEqual(response.status_code, 200)
        games = response.json().get('results')
        self.assertEqual(len(games), 30)


    def test_name_filter(self):
        for name in names:
            response = c.get('/api/games/all/', {'name': name, 'limit': '30'})
            self.assertEqual(response.status_code, 200)
            games = response.json().get('results')
            for game in games:
                self.assertTrue(name.lower() in game.get('name').lower())


    def test_genre_filter(self):
        for genre in genres:
            response = c.get('/api/games/all/', {'genres': '{' + genre + '}', 'limit': '30'})
            self.assertEqual(response.status_code, 200)
            games = response.json().get('results')
            for game in games:
                self.assertTrue(genre in game.get('genre'))


    def test_platforms_filter(self):
        response = c.get('/api/games/all/', {'platforms': '{\"windows\"}', 'limit': '30'})
        self.assertEqual(response.status_code, 200)
        games = response.json().get('results')
        for game in games:
            self.assertTrue('windows' in game.get('platforms'))

        response = c.get('/api/games/all/', {'platforms': '{\"windows\", \"mac\"}', 'limit': '30'})
        self.assertEqual(response.status_code, 200)
        games = response.json().get('results')
        for game in games:
            self.assertTrue('windows' in game.get('platforms'))
            self.assertTrue('mac' in game.get('platforms'))


    def test_popularity_order(self):
        response = c.get('/api/games/all/', {'popularity_order': 'ascending', 'limit': '30'})
        self.assertEqual(response.status_code, 200)
        games = response.json().get('results')
        for i in range(len(games) - 1):
            self.assertLessEqual(games[i].get('popularity'), games[i + 1].get('popularity'))

        response = c.get('/api/games/all/', {'popularity_order': 'descending', 'limit': '30'})
        self.assertEqual(response.status_code, 200)
        games = response.json().get('results')
        for i in range(len(games) - 1):
            self.assertGreaterEqual(games[i].get('popularity'), games[i + 1].get('popularity'))


    def test_price_filter(self):
        response = c.get('/api/games/all/', {'min_price': '500', 'max_price': '2000', 'limit': '30'})
        self.assertEqual(response.status_code, 200)
        games = response.json().get('results')
        for game in games:
            price = float(game.get('price_set')[0])
            self.assertGreaterEqual(price, 500)
            self.assertLessEqual(price, 2000)


    def test_price_order(self):
        response = c.get('/api/games/all/', {'price_order': 'ascending', 'limit': '30'})
        self.assertEqual(response.status_code, 200)
        games = response.json().get('results')
        for i in range(len(games) - 1):
            price_first = float(games[i].get('price_set')[0])
            price_second = float(games[i + 1].get('price_set')[0])
            self.assertLessEqual(price_first, price_second)

        response = c.get('/api/games/all/', {'price_order': 'descending', 'limit': '30'})
        self.assertEqual(response.status_code, 200)
        games = response.json().get('results')
        for i in range(len(games) - 1):
            price_first = float(games[i].get('price_set')[0])
            price_second = float(games[i + 1].get('price_set')[0])
            self.assertGreaterEqual(price_first, price_second)


    def test_rate_game(self):
        game = Game.objects.get(name='Greenwood the Last Ritual')
        id = game.steam_id
        response = c.post('/api/games/all/rate/', {'game': str(id), 'mark': '4'},
                          content_type='application/json')
        self.assertEqual(response.status_code, 200)
        game = Game.objects.get(name='Greenwood the Last Ritual')
        self.assertEqual(game.rating, 4.0)

        response = c.post('/api/games/all/rate/', {'game': str(id), 'mark': '100'},
                          content_type='application/json')
        self.assertEqual(response.status_code, 400)
        response = c.post('/api/games/all/rate/', {'game': str(id), 'mark': '0'},
                          content_type='application/json')
        self.assertEqual(response.status_code, 400)
