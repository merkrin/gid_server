
from django.urls import path
from .views import GameView, CommentView,  DiscountView, SingleGameView, PersonalCommentView,\
    delete_comment, ArticleView, SingleArticleView, rate_game


urlpatterns = [
    path('all/', GameView.as_view()),
    path('all/<int:pk>', SingleGameView.as_view()),
    path('all/<int:pk>/comments/', CommentView.as_view()),
    path('all/comments/personal/', PersonalCommentView.as_view()),
    path('all/rate/', rate_game),
    path('all/comments/delete/', delete_comment),
    path('discounts/', DiscountView.as_view()),
    path('articles/', ArticleView.as_view()),
    path('articles/<int:id>', SingleArticleView.as_view())
    ]
