import operator
from functools import reduce

from django.db.models import Min, Q
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.response import Response
from rest_framework import status
from .models import Game, Comment, Discount, Article
from users.models import User
from rest_framework.generics import get_object_or_404, ListCreateAPIView, ListAPIView, RetrieveAPIView
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import ListModelMixin
from .serializers import GameSerializer, CommentSerializer, DiscountSerializer, ArticleSerializer

from Gid_srv.logger import base_logger
logger = base_logger.getChild('games')

MAX_PRICE = 100000000
MIN_PRICE = 0


# awkward now, might be changed later
def filter_games(request):
    games = Game.objects.all()
    query = Q()
    logger.info("Filtering games according to request")

    if 'name' in request.GET:
        games = games.filter(name__icontains=request.GET.get('name'))

    if 'genres' in request.GET:
        query.add(reduce(operator.and_, (Q(genre__contains=x) for x in request.GET.getlist('genres'))), Q.AND)
    if 'shop' in request.GET:
        query.add((Q(shop__name__in=request.GET.getlist('shop'))), Q.AND)
    if 'platforms' in request.GET:
        query.add(reduce(operator.and_, (Q(platforms__contains=x) for x in request.GET.getlist('platforms'))), Q.AND)

    games = games.filter(query)

    try:
        if 'min_price' in request.GET or 'max_price' in request.GET:
            min_p = request.GET.get('min_price', MIN_PRICE)
            max_p = request.GET.get('max_price', MAX_PRICE)

            games = games.annotate(min_price=Min('price__price')).filter(min_price__range=(min_p, max_p))
    except ValueError:
        logger.error("Invalid price range")
        return Response({'error': 'Invalid price range'},
                        status=status.HTTP_400_BAD_REQUEST)

    if 'popularity_order' in request.GET:
        if request.GET['popularity_order'] == 'ascending':
            games = games.order_by('popularity')
        elif request.GET['popularity_order'] == 'descending':
            games = games.order_by('-popularity')
    elif 'price_order' in request.GET:
        if request.GET['price_order'] == 'ascending':
            games = games.annotate(min_price=Min('price__price')).order_by('min_price')
        elif request.GET['price_order'] == 'descending':
            games = games.annotate(min_price=Min('price__price')).order_by('-min_price')

    logger.info("Filtered successfully")
    return games


@permission_classes([permissions.AllowAny])
class GameView(ListModelMixin, GenericAPIView):
    # renderer_classes = [JSONRenderer]
    serializer_class = GameSerializer
    queryset = Game.objects.all()
    pagination_class = LimitOffsetPagination

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        logger.info("Received request for games")
        queryset = filter_games(request)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        logger.info("Responding normally")
        return Response(serializer.data)


class SingleGameView(RetrieveAPIView):
    queryset = Game.objects.all()
    serializer_class = GameSerializer
    pagination_class = LimitOffsetPagination


class CommentView(ListCreateAPIView):
    pagination_class = LimitOffsetPagination
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

    def list(self, request, *args, **kwargs):
        logger.info("Received request for comments on a game")
        game = self.kwargs['pk']
        queryset = Comment.objects.filter(game__pk=game)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        logger.info("Responding normally")
        return Response(serializer.data)

    def perform(self, request, serializer):
        game = get_object_or_404(Game, id=self.kwargs['pk'])
        user = request.user.id
        return serializer.save(game=game, user_id=user)

    def create(self, request, *args, **kwargs):
        logger.info("Received request to post a comment")
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        logger.info("Posting...")
        self.perform(request, serializer)
        logger.info("Posted successfully")
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class PersonalCommentView(ListModelMixin, GenericAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    pagination_class = LimitOffsetPagination

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        logger.info("Received request for comments of a specific user")
        user = request.user.id
        queryset = Comment.objects.all().filter(user_id=user)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        logger.info("Responding normally")
        return Response(serializer.data)


class DiscountView(ListAPIView):
    queryset = Discount.objects.all()
    serializer_class = DiscountSerializer
    pagination_class = LimitOffsetPagination

    def list(self, request, *args, **kwargs):
        logger.info("Received request for discounts")
        if 'personal' in request.GET:
            logger.info("Getting personal discounts")
            user = request.user
            queryset = Discount.objects.all().filter(price__game__in=user.wished_games.all())
        else:
            logger.info("Getting all discounts")
            queryset = self.get_queryset()

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        logger.info("Responding normally")
        return Response(serializer.data)


@api_view(["POST"])
def delete_comment(request):
    logger.info("Received request to delete a comment")
    comment_id = request.data.get('comment_id')
    user = request.user
    comment = Comment.objects.get(pk=comment_id)

    if comment is None:
        logger.error("Invalid ID: comment doesn't exist. Responding with 400")
        return Response("Invalid comment id", status=status.HTTP_400_BAD_REQUEST)
    if comment.user_id != user.id:
        logger.error("The user doesn't own this comment. Responding with 400")
        return Response("The user doesn't own this comment", status=status.HTTP_400_BAD_REQUEST)

    comment.delete()
    logger.info("Comment deleted successfully")
    return Response(status=status.HTTP_200_OK)


class ArticleView(ListModelMixin, GenericAPIView):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
    pagination_class = LimitOffsetPagination

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        logger.info("Received request for articles")
        # user-specific articles
        if 'personal' in request.GET:
            logger.info("Getting articles for user")
            user = request.user
            games = User.objects.get(id=user.id).wished_games.all()
            queryset = Article.objects.filter(game__in=games)
        # game-specific articles
        elif 'game' in request.GET:
            logger.info("Getting articles for game")
            game_id = request.GET['game']
            game = Game.objects.get(steam_id=game_id)
            queryset = Article.objects.filter(game=game)
        else:
            logger.info("Getting all articles")
            queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        logger.info("Responding normally")
        return Response(serializer.data)


class SingleArticleView(ListAPIView):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
    pagination_class = LimitOffsetPagination


@api_view(["POST"])
def rate_game(request):
    logger.info("Received request to rate a game")
    game_id = request.data.get('game')
    game = Game.objects.get(steam_id=game_id)
    if game is None:
        logger.error("No such game in the database. Responding with 404")
        return Response("No such game in the database", status=status.HTTP_404_NOT_FOUND)

    if not game.marks_numbers:
        game.marks_numbers = [0] * 4

    try:
        mark = int(request.data.get('mark'))
    except ValueError:
        logger.error("Mark is not a number. Responding with 400")
        return Response("Mark is not a number", status=status.HTTP_400_BAD_REQUEST)
    if mark < 1 or mark > 4:
        logger.error("Forbidden mark value. Responding with 400")
        return Response("Forbidden mark value", status=status.HTTP_400_BAD_REQUEST)

    total_marks = sum(game.marks_numbers)
    new_rating = (game.rating * total_marks + mark) / (total_marks + 1)
    game.marks_numbers[mark - 1] += 1
    game.rating = new_rating
    game.save()
    logger.info("Game rating updated successfully")
    return Response(status=status.HTTP_200_OK)
