# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from Gid_srv.config import game_news
from games.models import Game
from games.steam_articles_loader import parse_article_json
import json
from Gid_srv.logger import base_logger

logger = base_logger.getChild('steam_news_crawler')


class SteamNewsCrawlerSpider(CrawlSpider):
    name = 'steam_news_crawler'
    allowed_domains = ['api.steampowered.com']
    start_urls = [game_news]

    rules = (
        Rule(LinkExtractor(allow=r'Items/'), callback='parse_article', follow=True),
    )

    def start_requests(self):
        logger.info("Crawling Steam news...")
        games = Game.objects.all()
        for i in range(3):
        #for game in games:
            game = games[i]
            id = game.steam_id
            next_page = game_news + "?appid=" + str(id) + "&count=10&maxlength=500"
            yield scrapy.Request(next_page, callback=self.parse_article)


    def parse_article(self, response):
        if response.status != 200:
            logger.error("Failed to load: status code " + str(response.status_code))
            return None
        response_json = json.loads(response.text)
        news = response_json.get('appnews').get('newsitems')
        id = response_json.get('appnews').get('appid')
        if len(news) == 0:
            logger.warning("No news for this game")
            return None
        for item in news:
            parse_article_json(item, id)

