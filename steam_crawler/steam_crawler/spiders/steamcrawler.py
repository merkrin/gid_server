# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from games.steam_loader import parse_game_json
from Gid_srv.config import game_info, game_ids
import json
from Gid_srv.logger import base_logger

logger = base_logger.getChild('steam_crawler')


class SteamcrawlerSpider(CrawlSpider):
    name = 'steamcrawler'
    allowed_domains = ['api.steampowered.com', 'store.steampowered.com']
    start_urls = [game_ids]

    rules = (
        Rule(LinkExtractor(allow=r'Items/'), callback='parse_ids', follow=True),
    )

    def start_requests(self):
        logger.info("Crawling Steam games...")
        for url in self.start_urls:
            yield scrapy.Request(url=url, callback=self.parse_ids)

    def parse_ids(self, response):
        logger.info("parsing ids")
        if response.status != 200:
            logger.error("Failed to load: status code " + str(response.status_code))
            return None
        response_json = json.loads(response.text)

        apps = response_json.get("applist").get("apps")
        if len(apps) == 0:
            logger.warning("No games available now")
            return None

        # for debugging purposes change to smaller!
        for i in range(len(apps)):
            id = apps[i].get("appid")
            filters = 'basic,short_description,platforms,price_overview,metacritic,release_date,genres,developers,' \
                      'is_full,screenshots,movies,required_age,header_image,pc_requirements,detailed_description '
            next_page = game_info + "/?appids=" + str(id) + "&filters=" + filters + "&l=english"
            yield scrapy.Request(next_page, callback=self.parse_game)


    def parse_game(self, response):
        if response.status != 200:
            logger.error("Failed to load: status code " + str(response.status_code))
            return None
        response_json = json.loads(response.text)
        game_json = list(response_json.values())[0]
        if game_json.get('success'):
            logger.info("Game fetched, parsing json")
            parse_game_json(game_json)
        else:
            logger.warning("Game not in Steam database")
