from django.contrib.auth.forms import UserCreationForm
from django_registration.forms import RegistrationForm, User


class UserForm(UserCreationForm):
    class Meta(RegistrationForm.Meta):
        model = User
        fields = ['common_name', 'username', 'password', 'email', 'platforms']

