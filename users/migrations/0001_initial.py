# Generated by Django 3.0.2 on 2020-02-09 21:44

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('games', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('username', models.CharField(default='', max_length=30, unique=True)),
                ('login', models.CharField(default='', max_length=30)),
                ('email', models.EmailField(max_length=254, unique=True)),
                ('session_keys', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=100), blank=True, default=list, size=None)),
                ('platforms', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(choices=[('ps4', 'PlayStation 4'), ('ps3', 'Playstation 3'), ('xbox1', 'Xbox One'), ('xbox360', 'Xbox 360'), ('nintendo', 'Nintendo Switch'), ('mac', 'Mac'), ('windows', 'Windows'), ('linux', 'Linux')], max_length=15), default=list, size=None)),
                ('wished_games', models.ManyToManyField(blank=True, to='games.Game')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
