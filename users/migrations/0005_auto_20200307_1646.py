# Generated by Django 3.0.2 on 2020-03-07 16:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0004_auto_20200210_1740'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='session_keys',
        ),
        migrations.AddField(
            model_name='user',
            name='steam_key',
            field=models.CharField(default='', max_length=100),
        ),
    ]
