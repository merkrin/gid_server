# Generated by Django 3.0.2 on 2020-03-08 12:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0007_auto_20200308_1240'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='steam_key',
            field=models.CharField(blank=True, default='', max_length=100, null=True),
        ),
    ]
