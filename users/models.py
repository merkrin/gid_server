from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import UserManager, PermissionsMixin
from django.db import models
from django.contrib.postgres.fields import ArrayField
from games.models import Game, Platform


class MyUserManager(BaseUserManager):
    def create_user(self, email='', username='', common_name='', password=None):
        if not email or not username or not password:
            raise ValueError('No email/username/password')

        user = self.model(
            username=username,
            common_name=common_name,
            email=self.normalize_email(email),
            is_staff=False
        )
        user.save(using=self._db)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username='', password=None):
        if not username or not password:
            raise ValueError('No email/username/password')

        user = self.model(
            username=username,
            is_staff=True
        )
        user.save(using=self._db)
        user.set_password(password)
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    wished_games = models.ManyToManyField(Game, blank=True, default=list)
    username = models.CharField(max_length=30, default="", unique=True)
    common_name = models.CharField(max_length=30, default="")
    email = models.EmailField(unique=True)
    steam_id = models.CharField(max_length=100, default="", blank=True, null=True)
    platforms = ArrayField(models.CharField(max_length=15, choices=Platform.choices), default=list)
    session_key = models.CharField(max_length=200, default="", blank=True, null=True)
    recovery_code = models.PositiveIntegerField(default=0)
    verified_by_code = models.BooleanField(default=False)

    objects = MyUserManager()
    is_staff = models.BooleanField(default=False)

    USERNAME_FIELD = 'username'
    EMAIL_FIELD = 'email'

    def __str__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        return self.is_staff

    def has_perms(self, perm, obj=None):
        return self.is_staff

    def has_module_perms(self, app_label):
        return self.is_staff
