from rest_framework import serializers
from rest_framework.fields import empty

from .models import User
from games.serializers import GameSerializer


class UserSerializer(serializers.ModelSerializer):

    # def __init__(self, instance=None, data=empty, **kwargs):
    #     super().__init__(instance=None, data=empty, **kwargs)
    #     self.init_data = None

    wished_games = GameSerializer(many=True, required=False)

    class Meta:
        model = User
        fields = ('id', 'wished_games', 'username', 'common_name', 'email', 'platforms')

    def validate(self, data):
        if data['email'] is None or data['common_name'] is None or data['username'] is None:
            raise serializers.ValidationError("Login, common name and username must be not empty")

        return data


class SimpleUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'common_name', 'email', 'platforms')
