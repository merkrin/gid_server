from Crypto.Cipher import AES
from steam.client import SteamClient, EResult
from games.models import Game
from games.steam_loader import get_game_by_id_steam, parse_game_json
from Gid_srv.config import steam_key, owned_games, steam_profiles, steam_cookies
import requests
from enum import Enum
from Crypto.Random import get_random_bytes
from Crypto.Util.Padding import pad
from Gid_srv.logger import base_logger

logger = base_logger.getChild('steam_user')


class Problem(Enum):
    PASS = "INVALID_PASS"
    SERVICE = "SERVICE_UNAVAILABLE"
    AUTH_CODE = "NEED_AUTH_CODE"
    INV_AUTH_CODE = "INVALID_AUTH_CODE"
    FA = "NEED_2FA"
    INVALID_FA = "INVALID_2FA"
    UNKNOWN = "UNKNOWN_PROBLEM"
    STEAM_IS_BUSY = "STEAM_BOUND_TO_ANOTHER_ACCOUNT"
    STEAM_IS_BOUND = "ALREADY_BOUND"
    NOT_LOGGED_IN = "NOT_LOGGED_IN"


def get_user_owned_games(user):
    id = user.steam_id
    logger.info("Getting games for user " + str(id))
    steal_cookies(user)

    resp = requests.get(owned_games, params={'key': steam_key, 'steamid': str(id)})
    if resp.status_code != 200:
        logger.error("Failed to load: status code " + str(resp.status_code))
        return []
    games = []
    response = resp.json().get("response")
    count = response.get("game_count")
    if count is not None:
        for i in range(count):
            game_id = response.get("games")[i].get("appid")
            try:
                game = Game.objects.all().get(steam_id=game_id)
                logger.info("Fetched game " + str(game))
            except Game.DoesNotExist:
                logger.error("Not in GID database")
                continue
            games.append(game)
    logger.info("Fetched " + str(count) + " games")
    return games


def get_steam_ids(id):
    logger.info("Fetching wishlist IDs for user " + str(id))
    url = steam_profiles + str(id) + "/wishlistdata/?p=0"
    resp = requests.get(url, params={'p': 0})
    if resp.status_code != 200:
        logger.error("Failed to load: status code " + str(resp.status_code))
        return []
    ids = resp.json().keys()
    logger.info("Wшshlist IDs fetched")
    return ids


def get_user_wishlist(id):
    wished = []
    logger.info("Fetching wishlist for user " + str(id))
    ids = get_steam_ids(id)
    for game_id in ids:
        try:
            game = Game.objects.all().get(steam_id=game_id)
            logger.info("Fetched game " + str(game))
        except Game.DoesNotExist:
            logger.error("Not in GID database")
            continue
        wished.append(game)
    logger.info("Wishlist fetched successfully")
    return wished


# TODO: think about asynchronous check with crawlers
def update_user_wishlist(user):
    if not user.steam_id or user.steam_id == "":
        logger.error("Steam is not bound")
        return
    logger.info("Updating wishlist for user " + str(user.steam_id))
    steal_cookies(user)
    ids = get_steam_ids(user.steam_id)
    for game_id in ids:
        if not user.wished_games.filter(steam_id=game_id).exists():
            try:
                game = Game.objects.all().get(steam_id=game_id)
            except Game.DoesNotExist:
                logger.error("Not in GID database")
                continue
            user.wished_games.add(game)
            logger.info("Game " + str(game) + " added to wishlist")
            user.save()


def get_user_id(login, pswd, email_code=None, two_factor_code=None):
    logger.info("Trying to log in to Steam: user " + login)
    client = SteamClient()
    result = client.login(login, pswd, auth_code=email_code, two_factor_code=two_factor_code)

    if result == EResult.InvalidPassword:
        logger.error(Problem.PASS.value)
        raise BaseException(Problem.PASS.value)
    if result == EResult.ServiceUnavailable or result == EResult.TryAnotherCM:
        logger.error(Problem.SERVICE.value)
        raise BaseException(Problem.SERVICE.value)
    if result == EResult.AccountLogonDenied or result == EResult.InvalidLoginAuthCode:
        msg = Problem.AUTH_CODE.value if result == EResult.AccountLogonDenied else Problem.INV_AUTH_CODE.value
        logger.error(msg)
        raise BaseException(msg)
    if result == EResult.AccountLoginDeniedNeedTwoFactor or result == EResult.TwoFactorCodeMismatch:
        msg = (
            Problem.FA.value if result == EResult.AccountLoginDeniedNeedTwoFactor
            else Problem.INVALID_FA.value)
        logger.error(msg)
        raise BaseException(msg)

    if result == EResult.OK:
        enc_log = encrypt_login(pswd)
        logger.info("Logged in successfully")
        return client.steam_id.as_64, enc_log
    else:
        logger.error(Problem.UNKNOWN.value)
        raise BaseException(Problem.UNKNOWN.value)


def encrypt_login(login_key):
    with open("Gid_srv/secret.txt", "r") as secret_file:
        key = secret_file.read()

    key = bytes(key, 'utf-8')
    key = key.decode('unicode-escape').encode('ISO-8859-1').strip()

    iv = get_random_bytes(16)
    aes = AES.new(key, AES.MODE_CBC, iv)
    login_key = bytes(login_key, 'utf-8')
    encrypted = aes.encrypt(pad(login_key,16))

    return encrypted


def steal_cookies(user):
    steam_id = user.steam_id
    logger.info("Getting cookies for user " + str(steam_id))
    hashed = user.session_key
    with open("Gid_srv/secret.txt", "r") as secret_file:
        key = secret_file.read()
        key = bytes(key, 'utf-8')
        key = key.decode('unicode-escape').encode('ISO-8859-1').strip()
    resp = requests.get(steam_cookies, params={'steamid': steam_id, 'sessionkey': key, "encrypted_loginkey": hashed})
    # TODO: tell frontend to relogin in steam in this case
    if resp.status_code == 403:
        logger.error(Problem.NOT_LOGGED_IN.value)
        raise BaseException(Problem.NOT_LOGGED_IN.value)
    else:
        logger.info("Cookies obtained successfully")

