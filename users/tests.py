from django.test import TestCase
from django.test.utils import override_settings
from .models import User
from games.models import Game, Platform
from django.test import Client
from django.core import serializers
import json


class UserTestCase(TestCase):
    fixtures = ['fixtures/test_data.json']

    @override_settings(DEBUG=True)
    def setUp(self):
        self.client = Client()
        request = {'username': 'tester',
                   'password': 'topPass123',
                   'email': 'some_mail@mail.ru',
                   'common_name': 'me'}
        self.pswd = request['password']
        response = self.client.post('/api/users/register/', request,
                                    content_type='application/json')

        response = self.client.post('/api/users/login/', request,
                                    content_type='application/json')
        self.user = User.objects.all().get(username='tester')
        self.token = {'HTTP_AUTHORIZATION': 'Token ' + response.data['token']}

        self.game = Game(name='Life is strange', steam_id=0)
        self.game.save()

        User.objects.all().create(username='Nastya', email='grudina_1@mail.ru')
        User.objects.all().create(username='Masha', email='grudina_2@mail.ru')

    def test_simple(self):
        nastya = User.objects.all().get(username='Nastya')
        masha = User.objects.all().get(username='Masha')

        self.assertNotEqual(nastya, None)
        self.assertNotEqual(masha, None)
        # where is frontend???
        self.assertFalse(User.objects.all().filter(username='frontend').exists())

    def test_full_model(self):
        platforms = [Platform.LINUX, Platform.MAC]

        user = User(username='Sam', common_name='Sammy',
                    email='life_is_strange@yandex-team.ru', platforms=platforms)
        user.save()
        user.wished_games.add(self.game)
        user.save()

        self.assertTrue(User.objects.all().filter(username='Sam').exists())
        recovered = User.objects.all().get(username='Sam')
        wishlist = recovered.wished_games.all()
        self.assertTrue(wishlist.count() == 1)

        platforms = recovered.platforms
        self.assertTrue(platforms.__len__() == 2)

        self.assertEqual(recovered.common_name, 'Sammy')
        self.assertEqual(recovered.email, user.email)

    def test_auth(self):
        request = {'username': 'Masha',
                   'password': 'my_strong_pass',
                   'email': 'grudina_1@mail.ru',
                   'common_name': 'me'}

        response = self.client.post('/api/users/register/', request,
                                    content_type='application/json')
        print(response.data)
        # there can be only one Masha
        self.assertTrue(response.data["error"] == 'Duplicate username')

        request['username'] = 'Masha2'
        response = self.client.post('/api/users/register/', request,
                                    content_type='application/json')
        # email is already used
        self.assertTrue(response.data['error'] == 'Duplicate email')

        request['email'] = 'my_cool_mail@hse.ru'
        response = self.client.post('/api/users/register/', request,
                                    content_type='application/json')
        # pass is not alnum
        self.assertTrue(response.status_code == 400)

        request['password'] = 'short'

        response = self.client.post('/api/users/register/', request,
                                    content_type='application/json')
        # pass is short
        self.assertTrue(response.status_code == 400)

        request['password'] = 'finallyGoodPass12'
        response = self.client.post('/api/users/register/', request,
                                    content_type='application/json')
        self.assertTrue(response.status_code == 201)

    def test_login(self):
        request = {'username': 'vseleon',
                   'password': 'algebraTop',
                   'email': 'ya_pishu_eto_v_2_nochi@spat.hochu.ru',
                   'common_name': 'me'}
        response = self.client.post('/api/users/register/', request,
                                    content_type='application/json')
        self.assertTrue(response.status_code == 201)

        login_request = {'username': 'vseleon', 'password': 'algebraNeTop'}
        response = self.client.post('/api/users/login/', login_request,
                                    content_type='application/json')
        # really bad password
        print(response.data)
        self.assertTrue(response.status_code == 404)

        login_request['password'] = request['password']
        response = self.client.post('/api/users/login/', request,
                                    content_type='application/json')
        # really good password
        self.assertTrue(response.status_code == 200)

    def test_change_pass(self):
        request = {'old_pass': self.pswd, 'new_pass': 'bad'}
        response = self.client.post('/api/users/pass/', request,
                                    content_type='application/json', **self.token)
        self.assertTrue(response.data['error'] == 'Invalid new password')
        request['new_pass'] = 'pomogite1'
        request['old_pass'] = 'bad'
        response = self.client.post('/api/users/pass/', request,
                                    content_type='application/json', **self.token)
        self.assertTrue(response.data['error'] == 'Invalid old password')

        request['old_pass'] = self.pswd
        response = self.client.post('/api/users/pass/', request,
                                    content_type='application/json', **self.token)
        self.assertTrue(response.status_code == 200)

    def test_platforms(self):
        platforms = '{mac,linux,Unknown}'
        request = {'platforms': platforms}
        response = self.client.post('/api/users/platforms/', request,
                                    content_type='application/json', **self.token)
        self.assertTrue(response.data == 'Invalid platform Unknown')

        platforms = '{mac,linux,ps3}'
        request = {'platforms': platforms}

        response = self.client.post('/api/users/platforms/', request,
                                    content_type='application/json', **self.token)
        self.assertTrue(response.status_code == 200)

        self.user = User.objects.all().get(username='tester')
        print(self.user.platforms)
        self.assertTrue(self.user.platforms == ['mac', 'linux', 'ps3'])

    def test_wishlist_adding(self):
        request = {'game_id': -5}
        response = self.client.post('/api/users/wished/add/', request,
                                    content_type='application/json', **self.token)
        self.assertTrue(response.status_code == 400)

        request['game_id'] = 1111
        # doesn't exist
        response = self.client.post('/api/users/wished/add/', request,
                                    content_type='application/json', **self.token)
        self.assertTrue(response.status_code == 400)

        request['game_id'] = self.game.id
        response = self.client.post('/api/users/wished/add/', request,
                                    content_type='application/json', **self.token)
        print(response.data)
        self.assertTrue(response.status_code == 200)

    def test_wishlist_view(self):
        request = {'game_id': self.game.id}
        response = self.client.post('/api/users/wished/add/', request,
                                    content_type='application/json', **self.token)
        self.assertTrue(response.status_code == 200)

        response = self.client.get('/api/users/wished/', content_type='application/json', **self.token)
        # content = (response.content).decode('utf-8')
        content = json.loads(response.content)
        print(content)
        self.assertEqual(content[0]['name'], self.game.name)

    def test_unreleased(self):
        self.game.release_date = 'Feb 2019'
        self.game.save()
        request = {'game_id': self.game.id}
        response = self.client.post('/api/users/wished/add/', request,
                                    content_type='application/json', **self.token)
        self.assertTrue(response.status_code == 200)

        response = self.client.get('/api/users/wished/', content_type='application/json', **self.token)
        content = json.loads(response.content)

        self.assertTrue(len(content) == 1)

        response = self.client.get('/api/users/wished/?unreleased', content_type='application/json', **self.token)
        content = json.loads(response.content)
        self.assertTrue(len(content) == 0)

        self.game.release_date = '2021'
        self.game.save()
        response = self.client.get('/api/users/wished/?unreleased', content_type='application/json', **self.token)
        content = json.loads(response.content)

        self.assertTrue(len(content) == 1)

    def test_steam_login(self):
        user = User.objects.all().get(username='tester')
        self.assertEqual(user.steam_id, '')
        self.assertEqual(user.session_key, '')
        self.assertTrue(user.wished_games.count() == 0)

        request = {'login': 'aidenne6', 'password': 'grudina_top'}
        response = self.client.post('/api/users/steam/add/', request,
                                    content_type='application/json', **self.token)
        self.assertTrue(response.status_code == 200)

        user = User.objects.all().get(username='tester')
        self.assertNotEqual(user.steam_id, '')
        self.assertNotEqual(user.session_key, '')

        response = self.client.get('/api/users/wished/',
                                   content_type='application/json', **self.token)
        wished = json.loads(response.content)
        print(wished)
        self.assertTrue(len(wished) > 0)

    def test_steam_duplicate_login(self):
        request = {'login': 'aidenne6', 'password': 'grudina_top'}
        response = self.client.post('/api/users/steam/add/', request,
                                    content_type='application/json', **self.token)
        self.assertTrue(response.status_code == 200)

        login_request = {'username': 'meow', 'password': 'pobeda123', 'email': 'meow_mail@mail.ru',
                         'common_name': 'cat'}
        response = self.client.post('/api/users/register/', login_request,
                                    content_type='application/json', **self.token)

        response = self.client.post('/api/users/login/', login_request,
                                    content_type='application/json', **self.token)

        token = {'HTTP_AUTHORIZATION': 'Token ' + response.data['token']}

        response = self.client.post('/api/users/steam/add/', request,
                                    content_type='application/json', **token)

        self.assertTrue(response.status_code == 400)
        self.assertTrue(response.data == 'STEAM_BOUND_TO_ANOTHER_ACCOUNT')

    def test_user_view(self):
        self.user.is_staff = True
        self.user.save()
        response = self.client.get('/api/users/',
                         content_type='application/json', **self.token)

        users = json.loads(response.content)
        self.assertTrue(len(users) == 3)
        id = users[0]['id']

        response = self.client.get('/api/users/'+str(id),
                                   content_type='application/json', **self.token)

        self.assertTrue(response.status_code == 200)

        response = self.client.get('/api/users/' + str(12345),
                                   content_type='application/json', **self.token)
        self.assertTrue(response.status_code == 404)

