from django.urls import path
from .views import UserView, SingleUserView, create_auth, login, change_pass, \
    set_platforms, WishListView, add_game, log_in_steam, OwnedSteamGamesView, is_in_wishlist, send_verification_code, \
    verify_code, reset_password, get_self_info

urlpatterns = [
    path('', UserView.as_view()),
    path('personal/', get_self_info),
    path('<int:pk>', SingleUserView.as_view()),
    path('register/', create_auth),
    path('login/', login),
    path('pass/', change_pass),
    path('pass/recover/', send_verification_code),
    path('pass/verify_code/', verify_code),
    path('pass/reset/', reset_password),
    path('platforms/', set_platforms),
    path('wished/', WishListView.as_view()),
    path('wished/add/', add_game),
    path('wished/contains', is_in_wishlist),
    path('steam/add/', log_in_steam),
    path('steam/owned/', OwnedSteamGamesView.as_view())
]
