import datetime

from django.contrib.auth import authenticate
from rest_framework import permissions
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes

from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, GenericAPIView
from rest_framework.mixins import ListModelMixin
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, HTTP_200_OK
from rest_framework import status

from games.models import Platform, Game
from games.serializers import GameSerializer

from .models import User
from .serializers import UserSerializer, SimpleUserSerializer
from .steam_user import get_user_id, update_user_wishlist, get_user_owned_games, Problem

from django.core.mail import send_mail
import smtplib
import secrets
import dateparser

from Gid_srv.logger import base_logger

logger = base_logger.getChild('users')


class UserView(ListCreateAPIView):
    permission_classes = [permissions.IsAdminUser]
    queryset = User.objects.all()
    serializer_class = UserSerializer
    pagination_class = LimitOffsetPagination


class SingleUserView(RetrieveUpdateDestroyAPIView):
    # permission_classes = [permissions.IsAdminUser]
    queryset = User.objects.all()
    serializer_class = SimpleUserSerializer
    pagination_class = LimitOffsetPagination


def is_unreleased(game):
    now = datetime.datetime.utcnow()
    try:
        release = dateparser.parse(game.release_date, settings={'PREFER_DAY_OF_MONTH': 'last'})
    except Exception as e:
        logger.info('Failed to parse date for game %s: ' % game.name + str(e))
        return True

    return not release or release >= now


class WishListView(ListModelMixin, GenericAPIView):
    serializer_class = GameSerializer
    pagination_class = LimitOffsetPagination
    queryset = Game.objects.all()

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        logger.info("Received request for user's wishlist")
        user = request.user
        try:
            update_user_wishlist(user)
        except BaseException as ex:
            logger.error("Unknown exception occurred: " + str(ex))
            return Response(str(ex))

        queryset = user.wished_games.all()

        if 'unreleased' in request.GET:
            unreleased_ids = [game.id for game in queryset if is_unreleased(game)]
            queryset = queryset.filter(id__in=unreleased_ids)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        logger.info("Responding normally")
        return Response(serializer.data)


class OwnedSteamGamesView(ListModelMixin, GenericAPIView):
    serializer_class = GameSerializer
    pagination_class = LimitOffsetPagination
    queryset = Game.objects.all()

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        logger.info("Received request for user's owned Steam games")
        user = request.user

        if not user.steam_id or user.steam_id == "":
            logger.error("Steam account is not bound. Responding with 400")
            return Response("Steam account is not bound", status.HTTP_400_BAD_REQUEST)
        queryset = get_user_owned_games(user)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        logger.info("Responding normally")
        return Response(serializer.data)


@api_view(['POST'])
@permission_classes([permissions.AllowAny])
def create_auth(request):
    logger.info("Received request to register a user")
    serialized = UserSerializer(data=request.data)

    username = request.data.get('username')
    if User.objects.all().filter(username=username).exists():
        return Response({"error": "Duplicate username"},
                        status=status.HTTP_400_BAD_REQUEST)

    mail = request.data.get('email')
    if User.objects.all().filter(email=mail).exists():
        return Response({"error": "Duplicate email"},
                        status=status.HTTP_400_BAD_REQUEST)

    if serialized.is_valid():

        pswd = request.data.get('password')
        if len(pswd) < 8 or len(pswd) > 30 or not pswd.isalnum():
            logger.error("Invalid new password. Responding with 400")
            return Response({"error": "Invalid password"},
                            status=status.HTTP_400_BAD_REQUEST)
        User.objects.create_user(
            email=request.data.get('email'),
            username=request.data.get('username'),
            common_name=request.data.get('common_name'),
            password=request.data.get('password')
        )
        logger.info("Registered successfully")
        return Response(status=status.HTTP_201_CREATED)
    else:
        logger.error("Failed to register. Responding with 400")
        return Response({"error": "Invalid data"}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@permission_classes([permissions.AllowAny])
def login(request):
    logger.info("Received request to login")
    username = request.data.get("username")
    password = request.data.get("password")

    if username is None or password is None:
        logger.error("No username or password. Responding with 400")
        return Response({'error': 'No username or password'},
                        status=HTTP_400_BAD_REQUEST)

    user = authenticate(username=username, password=password)

    if not user:
        logger.error("Invalid credentials. Responding with 404")
        return Response({'error': 'Invalid Credentials'},
                        status=HTTP_404_NOT_FOUND)

    token, _ = Token.objects.get_or_create(user=user)
    logger.info("Logged in successfully. Returning a token. Responding normally")
    return Response({'token': token.key},
                    status=HTTP_200_OK)


@api_view(["POST"])
def change_pass(request):
    logger.info("Received request to change password")
    user = request.user
    old_password = str(request.data.get("old_pass"))
    new_password = str(request.data.get("new_pass"))

    if not user.check_password(old_password):
        logger.error("Invalid old password. Responding with 400")
        return Response({"error": "Invalid old password"},
                        status=status.HTTP_400_BAD_REQUEST)

    if len(new_password) < 8 or len(new_password) > 30 or not new_password.isalnum():
        logger.error("Invalid new password. Responding with 400")
        return Response({"error": "Invalid new password"},
                        status=status.HTTP_400_BAD_REQUEST)

    user.set_password(new_password)
    user.save()
    logger.info("New password was successfully set. Responding normally")
    return Response("New password was successfully set", status=status.HTTP_200_OK)


@api_view(["POST"])
def set_platforms(request):
    logger.info("Received request to change platforms")
    user = request.user
    platforms = request.data.get('platforms')
    platform_set = platforms[1:-1].split(',')

    for platform in platform_set:
        platform.strip()
        correct = False
        for k, v in Platform.choices:
            if k == platform:
                correct = True
                break
        if not correct:
            logger.error("Invalid platform " + platform + ". Responding with 400")
            return Response("Invalid platform " + platform, status=status.HTTP_400_BAD_REQUEST)

    user.platforms = platforms
    user.save()
    logger.info("New platforms set successfully: " + user.platforms + ". Responding normally")
    return Response("New platforms set successfully: " + user.platforms, status=status.HTTP_200_OK)


@api_view(["GET"])
def get_self_info(request):
    user = request.user
    serializer = SimpleUserSerializer(user)
    return Response(serializer.data)


@api_view(["POST"])
def add_game(request):
    logger.info("Received request to add a game for the user")
    user = request.user

    try:
        game_id = int(request.data.get('game_id'))
    except ValueError:
        logger.error("Game ID is not a positive integer. Responding with 400")
        return Response("ID must be a positive integer", status=status.HTTP_400_BAD_REQUEST)

    games = Game.objects.all()
    try:
        game = games.get(id=game_id)
    except Game.DoesNotExist:
        logger.error("Invalid game ID. Responding with 400")
        return Response("Invalid game ID", status=status.HTTP_400_BAD_REQUEST)

    user.save()
    game.save()

    user.wished_games.add(game)
    user.save()
    logger.info("Game " + str(game) + " successfully added. Responding normally")
    return Response("Game " + str(game) + " successfully added", status.HTTP_200_OK)


@api_view(["POST"])
def log_in_steam(request):
    logger.info("Received request to login to Steam")
    user = request.user
    login = request.data.get("login")
    pswd = request.data.get("password")
    auth_code = request.data.get("auth_code")
    fa = request.data.get("2fa")

    if user.steam_id != "" and user.steam_id:
        logger.error(Problem.STEAM_IS_BOUND.value + ". Responding with 400")
        return Response(Problem.STEAM_IS_BOUND.value, status.HTTP_400_BAD_REQUEST)
    if not login:
        logger.error("Missing login. Responding with 400")
        return Response("Missing login", status.HTTP_400_BAD_REQUEST)
    if not pswd:
        logger.error("Missing password. Responding with 400")
        return Response("Missing password", status.HTTP_400_BAD_REQUEST)

    key = None
    try:
        key, encrypted = get_user_id(login, pswd, auth_code, fa)
    except BaseException as e:
        logger.error("Unknown exception: " + str(e))
        return Response(str(e), status.HTTP_400_BAD_REQUEST)
    if User.objects.all().filter(steam_id=key).exists():
        logger.error(Problem.STEAM_IS_BUSY.value + ". Responding with 400")
        return Response(Problem.STEAM_IS_BUSY.value, status.HTTP_400_BAD_REQUEST)
    user.steam_id = key
    user.session_key = encrypted
    user.save()
    logger.info("Logged in to Steam successfully")
    return Response(status.HTTP_200_OK)


@api_view(["POST"])
def is_in_wishlist(request):
    logger.info("Received request to check if a game is in user's wishlist")
    user = request.user

    game = request.data.get("game_id")
    if not game:
        logger.error("No game id specified. Responding with 400")
        return Response("No game id specified", status.HTTP_400_BAD_REQUEST)

    update_user_wishlist(user)
    if user.wished_games.filter(id=game).exists():
        logger.info("Game is present in wishlist. Responding normally")
        return Response({'contains': 'true'}, status.HTTP_200_OK)
    else:
        logger.info("Game is not present in wishlist. Responding normally")
        return Response({'contains': 'false'}, status.HTTP_200_OK)


def create_message(code):
    msg = "Hello! You have requested to change your password for GID app. " \
          "Here's you account verification code: " + str(code) + ". " \
                                                                 "If you didn't request a password change, simply ignore this email."
    msg += "\n\n"
    msg += "Have a nice day!\nYour GID team"
    return msg


@api_view(["POST"])
def send_verification_code(request):
    logger.info("Received request to recover forgotten ")
    email = request.data.get('email')
    if email is None:
        logger.error("No email: email is required to reset password")
        return Response("Email is required to reset password", status.HTTP_400_BAD_REQUEST)

    user = User.objects.get(email=email)
    if user is None:
        logger.error("User is not registered in GID")
        return Response("User is not registered in GID", status.HTTP_400_BAD_REQUEST)

    code = secrets.randbelow(9000) + 1000
    user.recovery_code = code
    user.save()
    try:
        send_mail(subject="Recover your password",
                  message=create_message(code),
                  from_email="gidapp.noreply@gmail.com",
                  recipient_list=[email],
                  fail_silently=False)
    except smtplib.SMTPException as e:
        logger.error("Failed to send email. Reason: " + str(e))
        return Response("Failed to send email. Reason: " + str(e), status.HTTP_500_INTERNAL_SERVER_ERROR)

    logger.info("Email with the recovery code has been sent")
    return Response("Email with the recovery code has been sent", status.HTTP_200_OK)


@api_view(["POST"])
def verify_code(request):
    logger.info("Received request to verify account with email code")
    sent_code = request.data.get('code')
    email = request.data.get('email')

    if sent_code is None or email is None:
        logger.error("No email or verification code")
        return Response("Email and verification code required", status.HTTP_400_BAD_REQUEST)

    try:
        received_code = int(sent_code)
    except ValueError:
        logger.error("Invalid verification code: not a number")
        return Response("Invalid verification code: not a number", status.HTTP_400_BAD_REQUEST)

    user = User.objects.get(email=email)
    if user is None:
        logger.error("User is not registered in GID")
        return Response("User is not registered in GID", status.HTTP_400_BAD_REQUEST)

    if user.recovery_code == 0:
        logger.error("Password recovery wasn't requested")
        return Response("Password recovery wasn't requested", status.HTTP_400_BAD_REQUEST)

    if user.recovery_code != received_code:
        logger.error("Incorrect verification code")
        return Response("Incorrect verification code", status.HTTP_400_BAD_REQUEST)

    user.verified_by_code = True
    user.recovery_code = 0
    user.save()
    logger.info("Account verified successfully")
    return Response("Account verified successfully", status.HTTP_200_OK)


@api_view(["POST"])
def reset_password(request):
    logger.info("Received request to reset password")
    email = request.data.get('email')
    new_password = request.data.get('new_password')
    if email is None or new_password is None:
        logger.error("No email or new password")
        return Response("Email and new password required", status.HTTP_400_BAD_REQUEST)

    user = User.objects.get(email=email)
    if user is None:
        logger.error("User is not registered in GID")
        return Response("User is not registered in GID", status.HTTP_400_BAD_REQUEST)

    if not user.verified_by_code:
        logger.error("Verification failed, password can't be reset")
        return Response("Verification failed, password can't be reset", status.HTTP_400_BAD_REQUEST)

    if len(new_password) < 8 or len(new_password) > 30 or not new_password.isalnum():
        logger.error("New password doesn't satisfy password requirements")
        return Response("New password must consist of 8 to 30 alphanumeric characters", status.HTTP_400_BAD_REQUEST)
    if user.check_password(new_password):
        logger.error("New password is the same as the old one")
        return Response("Old and new password can't be the same", status.HTTP_400_BAD_REQUEST)

    user.set_password(new_password)
    user.verified_by_code = False
    user.save()
    logger.info("Password reset successfully")
    return Response("Password reset successfully", status.HTTP_200_OK)
